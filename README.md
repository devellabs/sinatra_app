Sinatra
============

Simple Sinatra app that uses:
 - ERB
 - AngularJS
 - MongoDb
 - Redis


installation:

    cd sinatra_app

    bundle install

    bundle exec ruby app.rb 
